﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace TestApps
{
    class Program
    {
        static void Main(string[] args)
        {
            Program test = new Program();
            int retval = test.solution(37);
            Console.WriteLine(retval);
            Console.ReadKey();
        }

       public int solution(int N)
        {
            // write your code in C# 6.0 with .NET 4.5 (Mono)
          
            string binary = Convert.ToString(N, 2);

           var charArray = binary.ToCharArray();

            int max = 0;
            int previousMax = 0;
            bool start = false;

            for(int x = 0; x < charArray.Length; x++)
            {
                if (charArray[x] == '1')
                {
                    start = true;

                        if (max > previousMax)
                        {
                            previousMax = max;
                            max = 0;
                        }
                }
                else
                {
                    if (start == true)
                    {
                        max++;
                    }
                }

            }

            return previousMax;
        }

    }
}
